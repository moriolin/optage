# optage


## Build Setup


``` bash
# 依存関係のインストール
$ npm install # Or yarn install

# ローカルサーバーとホットリロードの起動 localhost:3000
$ npm run dev


#ここまで基本的な操作


# build for production and launch server
$ npm run build
$ npm start

# 静的ジェネレーター機能
$ npm run generate
```

詳しくはこちらをご参照ください [Nuxt.js docs](https://github.com/nuxt/nuxt.js).







## 各パーツについて

###ラジオボタン
ラジオコンポーネントの作成は、変数をRadioのvモデルにバインドするだけです。
```html
<template>
  <el-radio v-model="radio" label="1">Option A</el-radio>
  <el-radio v-model="radio" label="2">Option B</el-radio>
</template>
```

```
export default {
  data () {
    return {
      radio: '1'
    };
  }
}
```


###スライダー
```html
<el-slider
        v-model="value_slider3"
        :step="33.33"
        :show-tooltip="false"
        @change="onSliderChange('slider3', value_slider3)"
        show-stops>
</el-slider>
```
v-model：スライダーのID  
:step：一メモリあたりの進度  
:show-tooltip：ツールチップは使わないので、defaultから変更しない  
@change：つまみを動かしたときの動作  


```
  data() {
    return {
      //slider　初期値の設定（単位は%）
      value_slider0: 0,
      
      //　スライダーを変更の際つまみの色を変わる
      chgFlg0 : false
    }
  },

  methods: {
    onSendClick() {
      // スライダーのつまみの色をもとに戻す
      this.chgFlg0 = false
    },

    // 省エネレベルとのバインディング
    onSliderChange(slidername, value){
      var percent = this.convertValues(Math.round(value));

      switch(slidername){
        case "slider0" :
          this.energyLevelMeter0.value = percent;
          this.energyLevelMeter0.status = this.getStatusByPercent(percent);
          this.chgFlg0 = true;
        break;
      }
    }
  }
```

設定送信ボタン押下でスライダーのつまみの色をもとに戻しています。



###店舗側ロック
```html
<el-switch
        v-model="value_rock4"
        active-color="#13ce66"
        inactive-color="#ddd">
</el-switch>
```


```
data() {
  return {
    //rock 初期値の設定
    value_rock0: false,
  }
},
```


###パンくず
```html
<content-main>

  <div class="headline f-flex">
    <breadcrumb :breadcrumbs="breadcrumbs"></breadcrumb>
  </div>
```

```
data() {
  return {
    breadcrumbs: [
      {
        name: 'メニュー一覧',
        path: '/menu/'
      },
      {
        name: '第2階層のページ名',
        path: '/パス/'
      },
    ],
  }
},
```


点滅する文字色は_settings.scssにあります。
他の色が必要な場合は、ここに追記してお使いください。
```scss
//点滅
.blink_text-red {
  animation: blinkAnime_text-red 0.6s infinite alternate;
}

@keyframes blinkAnime_text-red {
  0% { color: #6d6d6d }
  100% { color: #ff0000 }
}
```

スライダーのつまみの色は_settings.scssにあります。
他の色が必要な場合は、ここに追記してお使いください。
```scss
//スライダーのつまみの色
.wrap_el_slider {
  &.bg_slider-purple {
    .el-slider__button {
      background-color: #CA94F8;
    }
  }
}
```



その他の各コンポーネントの詳しい使い方はこちらをご参照ください [element UI component](https://element.eleme.io/#/en-US/component/slider).